# Especialización en desarrollo FullStack
Desarrollo FullStack por la universidad Austral, cursado en Coursera.

## Desarrollo de páginas con Angular - Evaluación del proyecto - Módulo 2

### Ejercicios
1. Implementar arquitecturas basadas en componentes.
2. Crear formularios interactivos
3. Crear aplicaciones reactivas utilizando Redux

---

### Todo web application
Una aplicación web responsive desarrollada principalmente con el framework de Angular con una lista de tareas por hacer y peliculas pendientes por ver

### Para empezar

Para satisfacer las dependencias del proyecto, ejecute:
```bash
npm install
```

Para arrancar la aplicación
```bash
ng serve
```

### Tecnologías
- HTML / CSS / JS
- Angular
- Redux
- TypeScript
- Bootstrap 4
- NodeJS
- Visual Studio Code
- Git
- Bitbucket
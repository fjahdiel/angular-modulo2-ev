import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TodoListComponent } from './todo-list/todo-list.component';
import { TodoItemComponent } from './todo-item/todo-item.component';
import { TodoHistoryDataComponent } from './todo-history-data/todo-history-data.component';
import { AddTodoItemFormComponent } from './add-todo-item-form/add-todo-item-form.component';
import { TodoMoviesToWatchComponent } from './todo-movies-to-watch/todo-movies-to-watch.component';
import { TodoMovieItemComponent } from './todo-movie-item/todo-movie-item.component';
import { reducers } from './store/reducers/main-reducer';

import { StoreModule as NgRxStoreModule } from '@ngrx/store';

const routes: Routes = [
 { path: '', redirectTo: 'home', pathMatch: 'full' },
 { path: 'home', component: TodoListComponent },
 { path: 'movies', component: TodoMoviesToWatchComponent },
 { path: 'history', component: TodoHistoryDataComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    TodoListComponent,
    TodoItemComponent,
    TodoHistoryDataComponent,
    AddTodoItemFormComponent,
    TodoMoviesToWatchComponent,
    TodoMovieItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    NgRxStoreModule.forRoot(reducers)
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }

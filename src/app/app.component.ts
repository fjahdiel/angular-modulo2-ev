import { Component } from '@angular/core';
import { Router, NavigationEnd, Event } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'todo-app';

  constructor(private router: Router) {
    router.events.subscribe( (event: Event) => {
      if (event instanceof NavigationEnd) {

        const navHome = document.getElementById('nav_home');
        const navHistory = document.getElementById('nav_history');
        const navMovies = document.getElementById('nav_movies');

        switch (event.urlAfterRedirects) {
          case '/home': {
            navHome.classList.add('active');
            navHistory.classList.remove('active');
            navMovies.classList.remove('active');
            break;
          }
          case '/history': {
            navHome.classList.remove('active');
            navHistory.classList.add('active');
            navMovies.classList.remove('active');
            break;
          }
          case '/movies': {
            navHome.classList.remove('active');
            navHistory.classList.remove('active');
            navMovies.classList.add('active');
          }
        }
      }
    });
  }

}

import { Action } from '@ngrx/store';
import { TodoItem } from './../../models/todo-item.models';

// ACTIONS
export enum TodoItemsActionTypes {
    NEW_TODO_ITEM = '[Todo Item] NEW',
    DELETE_TODO_ITEMS = '[Todo Item] DELETE ITEMS',
    ADD_SELECTED_TODO_ITEM = '[Todo Item] ADD SELECTED ITEM',
    DELETE_SELECTED_TODO_ITEM = '[Todo Item] DELETE SELECTED ITEM'
}

export class NewTodoItemAction implements Action {
    type = TodoItemsActionTypes.NEW_TODO_ITEM;
    constructor(public todoItem: TodoItem) {}
}

export class DeleteTodoItemsAction implements Action {
    type = TodoItemsActionTypes.DELETE_TODO_ITEMS;
    constructor() {}
}

export type TodoItemsActions = NewTodoItemAction | DeleteTodoItemsAction;

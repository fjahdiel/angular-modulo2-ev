import { Component, OnInit } from '@angular/core';
import { TodoMovieItem } from '../models/todo-movie-item.models';
import { TodoMoviesApiClient } from '../models/todo-movies-api-client.model';

@Component({
  selector: 'app-todo-movies-to-watch',
  templateUrl: './todo-movies-to-watch.component.html',
  styleUrls: ['./todo-movies-to-watch.component.css'],
  providers: [ TodoMoviesApiClient ]
})
export class TodoMoviesToWatchComponent implements OnInit {

  constructor(public apiClient: TodoMoviesApiClient) {
  }

  ngOnInit() {
    
  }
}
